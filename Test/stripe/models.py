from django.db import models

class Product(models.Model):
	productName = models.CharField(max_length=50)
	# tripName = models.CharField(max_length=50)
	# starsNumber = models.IntegerField(default=0)
	price = models.IntegerField(default=0)
	# quantity = models.IntegerField(default=0)
	# earnedPoints = models.IntegerField(default=0)

	def __str__(self):
		return self.productName

	def get_display_price(self):
		return "{0:.2f}".format(self.price/100)
